---
name: Feature
about: New features or wider changes, 

---

**Description**
A brief description of the PR.

**Changelog**
Please summarize the changes in one a list to generate the changelog:
E.g.
- Added feature X to Y (#XXXX)
- Added Foo Application
